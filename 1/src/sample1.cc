#include <iostream>
#include <cmath>
#include "sample1.h"

MyVector::MyVector(const int& x,const int& y,const int& z):
        x(x),
        y(y),
        z(z)
    {}

MyVector::MyVector():
        x(0),
        y(0),
        z(0)
    {}
int MyVector::getX() const{
    return x;
}

int MyVector::getY() const{
    return y;
}

int MyVector::getZ() const{
    return z;
}

float MyVector::getLength() const{
    return sqrt(x*x+y*y+z*z);
}

void MyVector::setY(int exceptY) {
    y = exceptY;
}

void MyVector::setX(int exceptX) {
    x = exceptX;
}

void MyVector::setZ(int exceptZ) {
    z = exceptZ;
}
MyVector operator +(const MyVector& vector1,const MyVector& vector2)
{
    return MyVector(vector1.getX()+vector2.getX(),vector1.getY()+vector2.getY(),vector1.getZ()+vector2.getZ());
}

MyVector operator -(const MyVector& vector1,const MyVector& vector2)
{
    return MyVector(vector1.getX()-vector2.getX(),vector1.getY()-vector2.getY(),vector1.getZ()-vector2.getZ());
}

std::istream& operator >>(std::istream& ins, MyVector& vector)
{
    int x,y,z;
    ins >> x >> y >> z;

    vector.setX(x);
    vector.setY(y);
    vector.setZ(z);

    return ins;

}
