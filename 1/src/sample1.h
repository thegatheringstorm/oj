#include <iostream>
#include <cstdlib>
#ifndef GTEST_SAMPLES_SAMPLE1_H_
#define GTEST_SAMPLES_SAMPLE1_H_
class MyVector{
private:
    int x;
    int y;
    int z;
    float length;

    void setX(int);
    void setY(int);
    void setZ(int);
public:
    MyVector(const int& x,const int& y,const int& z);
    MyVector();

    int getX() const;
    int getY() const;
    int getZ() const;

    float getLength() const;

    friend MyVector operator +(const MyVector& vector1,const MyVector& vector2);
    friend MyVector operator -(const MyVector& vector1,const MyVector& vector2);
    friend std::istream& operator >>(std::istream& is, MyVector& vector);


};
#endif  // GTEST_SAMPLES_SAMPLE1_H_
