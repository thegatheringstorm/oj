// Step 1. Include necessary header files such that the stuff your
// test logic needs is declared.
//
// Don't forget gtest.h, which declares the testing framework.

#include <limits.h>
#include <iostream>
#include "../src/sample1.h"
#include "gtest/gtest.h"

TEST(MyVectorTest, initVector) {
  MyVector a(1,2,3);
  EXPECT_EQ(1, a.getX());
  EXPECT_EQ(2, a.getY());
  EXPECT_EQ(3, a.getZ());
}

TEST(MyVectorTest, getVectorLength) {
  MyVector a(0,3,4);
  EXPECT_EQ(5, a.getLength());
}

TEST(MyVectorTest, VectorPlus) {
  MyVector a(0,1,2);
  MyVector b(0,2,2);
  MyVector c;
  c = a+b;
  EXPECT_EQ(3, c.getY());
  EXPECT_EQ(5, c.getLength());
}
/*
TEST(MyVectorTest, Operator>>) {
  MyVector c;
  std::cin >> c;
  EXPECT_EQ(3, c.getX());
  EXPECT_EQ(4, c.getY());
  EXPECT_EQ(5, c.getZ());
}*/