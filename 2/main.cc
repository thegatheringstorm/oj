#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <cmath>

using namespace std;

class MyComplex{
private:
    int x;
    int y;


    void setX(int);
    void setY(int);

public:
    MyComplex(const int& x,const int& y);
    MyComplex();

    int getX() const;
    int getY() const;
    string getRealPart() const;
    string getImaginaryPart() const;
    string getSeparator() const;


    friend MyComplex operator +(const MyComplex& complex1,const MyComplex& complex2);
    friend MyComplex operator -(const MyComplex& complex1,const MyComplex& complex2);
    friend std::istream& operator >>(std::istream& is, MyComplex& complex);
    friend std::ostream& operator <<(std::ostream& os, MyComplex& complex);


};
MyComplex::MyComplex(const int& x,const int& y):
        x(x),
        y(y)
    {}

MyComplex::MyComplex():
        x(0),
        y(0)
    {}
int MyComplex::getX() const{
    return x;
}

int MyComplex::getY() const{
    return y;
}

string MyComplex::getRealPart() const{
    string str {};
    ostringstream oss;
    if(x) oss <<  x;
    str = oss.str();

    return str;
}

string MyComplex::getImaginaryPart() const{
    string str {};
    ostringstream oss;

    if(y) oss << abs(y) << "i";
    str = oss.str();

    return str;
}

string MyComplex::getSeparator() const{
    string str {};
    ostringstream oss;

    if(!(x==0 && y>=0)) {
        if(y>0) oss << "+";
        else if(y<0) oss << "-";
    }

    str = oss.str();
    return str;
}

void MyComplex::setY(int exceptY) {
    y = exceptY;
}

void MyComplex::setX(int exceptX) {
    x = exceptX;
}

MyComplex operator +(const MyComplex& complex1,const MyComplex& complex2)
{
    return MyComplex(complex1.getX()+complex2.getX(),complex1.getY()+complex2.getY());
}

MyComplex operator -(const MyComplex& complex1,const MyComplex& complex2)
{
    return MyComplex(complex1.getX()-complex2.getX(),complex1.getY()-complex2.getY());
}

istream& operator >>(istream& ins, MyComplex& complex)
{
    int x,y;
    ins >> x >> y;

    complex.setX(x);
    complex.setY(y);

    return ins;

}
ostream& operator <<(ostream& os, MyComplex& complex)
{
    if(complex.getX()==0 && complex.getY()==0) os << 0;
    else os << complex.getRealPart()<<complex.getSeparator() <<complex.getImaginaryPart();

    return os;

}
int main()
{
    MyComplex a,b,c,d;
    while(cin>>a>>b) {
        c = a+b;
        d = a-b;
        cout << c << endl;
        cout << d << endl;
    }
    return 0;
}