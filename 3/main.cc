#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
istream& operator >>(istream& ins, std::vector<int>& vec)
{
    int x;
    ins >> x;
    vec.push_back(x);

    return ins;
}
int main(void)
{
    int caseCount {};
    cin >> caseCount;
    for (int i=1;i<=caseCount;i++)
    {
        std::vector<int> week {};
        for(int p=1;p<=7;p++) cin >> week;
        vector<int>::iterator max = std::max_element(std::begin(week), std::end(week));
        vector<int>::iterator min = std::min_element(std::begin(week), std::end(week));
        int sum = std::accumulate(week.begin() , week.end(),0);
        cout << *max<<endl;
        cout << *min<<endl;
        cout << sum/7<<endl;

    }
    return 0;
}