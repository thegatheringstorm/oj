#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

class MyVector{
private:
    int x;
    int y;

    float length;

    void setX(int);
    void setY(int);

public:
    MyVector(const int& x,const int& y);
    MyVector();

    int getX() const;
    int getY() const;


    float getLength() const;

    friend MyVector operator +(const MyVector& vector1,const MyVector& vector2);
    friend MyVector operator -(const MyVector& vector1,const MyVector& vector2);
    friend std::istream& operator >>(std::istream& is, MyVector& vector);


};
MyVector::MyVector(const int& x,const int& y):
        x(x),
        y(y)
    {}

MyVector::MyVector():
        x(0),
        y(0)
    {}
int MyVector::getX() const{
    return x;
}

int MyVector::getY() const{
    return y;
}



float MyVector::getLength() const{
    return sqrt(x*x+y*y);
}

void MyVector::setY(int exceptY) {
    y = exceptY;
}

void MyVector::setX(int exceptX) {
    x = exceptX;
}

MyVector operator +(const MyVector& vector1,const MyVector& vector2)
{
    return MyVector(vector1.getX()+vector2.getX(),vector1.getY()+vector2.getY());
}

MyVector operator -(const MyVector& vector1,const MyVector& vector2)
{
    return MyVector(vector1.getX()-vector2.getX(),vector1.getY()-vector2.getY());
}

std::istream& operator >>(std::istream& ins, MyVector& vector)
{
    int x,y;
    ins >> x >> y;

    vector.setX(x);
    vector.setY(y);

    return ins;

}

int main()
{
    int caseCount;
    cin >> caseCount;
    MyVector a, b, c;

    for (int i = 1; i <= caseCount; i++)
    {
        cin >> a >> b;
        c = b-a;
        cout << fixed << setprecision(3) << c.getLength();
        cout << " ";
        cout << c.getX() * c.getY();
        cout << endl;
    }

    return 0;
}
